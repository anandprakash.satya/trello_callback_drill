const fs = require('fs')
const path = require('path')

function problem1(id,callback1) {
    setTimeout(()=>{
        fs.readFile(path.join(__dirname,'./data/boards.json'),'utf-8',(err,data)=>{
            if(err){
                console.log(err)
            }
            else{
                let result=JSON.parse(data)
                let board=result.find(item=>{
                    return item.id==id
                })
                if(board){
                    callback1(null,board)
                }else{
                    callback1(new Error('board not found'))
                }
                
            }
        })
    },2*1000)
}
module.exports=problem1