const fs = require('fs')
const path = require('path')

function problem2(id,callback2) {
    setTimeout(()=>{
        fs.readFile(path.join(__dirname,'./data/lists.json'),'utf-8',(err,data)=>{
            if(err){
                console.log(err);
            }else{
                let result = JSON.parse(data)
                let listId = result[id]
                if(listId){
                    callback2(null,listId)
                }else{
                    callback2(new Error('list not found'))
                }
            }
        })
    }, 2*1000)
    
}

module.exports=problem2