const fs=require('fs')
const path=require('path');

function problem3(id,callback3){
    setTimeout(() => {
        fs.readFile(path.join(__dirname,'./data/cards.json'),'utf-8',(err,data)=>{
            if(err){
                console.log(err);
            }else{
                let result=JSON.parse(data)
                let cardId = callback3(result[id])
                return cardId
            }
        })
    }, 2*1000);
}
module.exports=problem3