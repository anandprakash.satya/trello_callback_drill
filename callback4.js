const fs = require("fs");
const path = require("path");
const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

function problem4(name){
    setTimeout(()=>{
        fs.readFile(path.join(__dirname,"./data/boards.json"),'utf-8',(err,data)=>{
            if(err){
                console.log(err);
            } else {
                let res = JSON.parse(data).filter((elem)=>{
                    return elem.name == name;
                })
                let id = res[0].id;
                // console.log(id);
                callback1(id, (err,data)=>{
                    if(err){
                        console.log(err);
                    } else {
                        console.log(data);

                        callback2(id, (err,data)=>{
                            if(err){
                                console.log(err);
                            } else {
                                console.log(data);

                                let mindData = data.filter((index)=>{
                                    return index.name == "Mind"
                                });

                                let mindDataId = mindData[0].id;

                                callback3(mindDataId,(err,data)=>{
                                    if(err){
                                        console.log(err);
                                    } else {
                                        console.log(data);
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })

    },2 * 1000);
}

module.exports = problem4;

